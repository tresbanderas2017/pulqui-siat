# Pulqui - Siat

En source (src) tenemos las carpetas:

components:
          Aquí se encuentran los contenidos de las páginas.
          App es todo, y aqui tiene pulquiConteiner. Y despues están Main, noticias y actividades.

config:
      Util para definir datos y luego utilizarlos. No es obligatoria, los unicos obligatorios son routing y components.

dist:
    Aqui queda la version estable luego de compilarla mediante consola con: ../node_modules/.bin/webpack .

menu:
    Aqui se define un json con los datos del menu

routing:
        Aqui se definen las rutas dos veces. Una directamente por si alguien accede a una dirección, y otra mediante route2Component donde es interpretado mediante la navegacion interna. (esto se va a simplificar)
----------------------------------------------------------------------------------------------

Lo primero que se interpreta es src/index.js que llama a <App /> (App es toda la página).
El componente App solamente devuelve el componente <PulquiContainer> con el parametro llamado component y este parametro es el componente <Main> (calculado por route2Component(currentRoute), el cual según la URL es el dato que obtiene, por ello al principio para la ruta inicial es Main).

Main, Actividades y Noticias de components poseen la generación del HTML de lo que sería en el SIAT el <div ="idContenido">, que en este prototipo es el siguiente: <main class="mdl-layout__content">
