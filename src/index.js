import { Pulqui } from '../lib/pulqui.min.js'

import App from './components/App/App'

let miPulquiApp = Pulqui()
miPulquiApp.start(
		<App />,
		document.getElementById('pulqui-app')
)
