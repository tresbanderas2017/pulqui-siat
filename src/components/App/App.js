import yo from 'yo-yo'

import config from '../../config'
import menu from '../../menu'

import { route2Component } from '../../routing'

function PulquiContainer(props) {

	function render(state) {

		return (
			<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">

				<header style="z-index: 0" class="mdl-layout__header">
					<div class="mdl-layout__header-row">
						{
							props.titulo?
							<span className="mdl-layout-title">Pulqui - Siat | {props.titulo}</span>:
							<span className="mdl-layout-title">Pulqui - Siat</span>
						}
						<div>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right">
								<div class="mdl-textfield__expandable-holder">
									<input class="mdl-textfield__input" type="text" name="sample" id="fixed-header-drawer-exp" />
								</div>
							</div>
						</div>
						<div class="mdl-layout-spacer"></div>
						<label class="mdl-button mdl-button--icon"></label>
					</div>
				</header>

				<div class="mdl-layout__drawer">
					<nav class="mdl-navigation">
						<aside class="menu">
							{
								props.opciones.map( opcion => {
									return (
										<a id={opcion.etiqueta} class="mdl-navigation__link" href={opcion.ruta} onclick={props.seleccionarOpcion}>{opcion.etiqueta}</a>
									)
								})
							}
						</aside>
					</nav>
				</div>

				<main class="mdl-layout__content">
					<div class="page-content">
						{/*En este elemento main se muestra el componente que llega en el parametro*/}
						{/*y se llama component, en este caso <Home>*/}
						<br />
						<main id="pulqui-container">
							{ props.component }
						</main>
					</div>
				</main>

				<footer class="mdl-mini-footer">
					<div class="mdl-mini-footer__left-section">
						<p>UNRC</p>
					</div>
				</footer>

			</div>
		)
	}

	const component = render()
	return component
}


//El componente App solamente devuelve el componente
//<PulquiContainer> con el parametro llamado component
//y este parametro es el componente <Main>
function App(props) {

	function handleChangeMenu(e) {
		const titulo = e.target.id

		state.titulo = titulo

		const updated = render(state)
		yo.update(component, updated)

		//Específico de MDL para reaplicar las clases
    componentHandler.upgradeAllRegistered()
	}


	function render(state) {
		const currentRoute = window.location.pathname + window.location.hash
		const component = route2Component(currentRoute)

		return(
				<PulquiContainer	opciones={state.opciones}
													seleccionarOpcion={handleChangeMenu}
													titulo={state.titulo}
													component={component}
				/>
		)
	}

	let state = {}

	state.menu = menu
	state.opciones = menu[0].opciones
	state.titulo = null

	const component = render(state)
	return component
}

export default App
