import yo from 'yo-yo'

import config from '../../config'

function Noticias() {

	function render() {
		return(
			<div id="noticias">
				<p class="title">Noticias</p>
			</div>
		)
	}

	fetch(config.jspNoticias)
		.then(textResponse => textResponse.text() )
		.then(response => {
				const elem = document.getElementById('noticias')
				if(!elem) return
				elem.innerHTML = response
			}
		)

	const component = render()
	return component
}

export default Noticias
