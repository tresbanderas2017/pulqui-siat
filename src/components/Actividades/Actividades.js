import yo from 'yo-yo'

import config from '../../config'

function Actividades() {

	function render() {
		return(
			<div id="actividades">
				<p class="title">Actividades</p>
			</div>
		)
	}


	fetch(config.jspActividades)//fecth esta basado en promesas.
		//promesas (promises), un valor que posiblemente sea resuelto o rechazado
		.then(textResponse => textResponse.text() )//textResponse es la respuesta de la promesa fecth
		.then(response => {//response es la respuesta de la promesa textResponse.text()
				const elem = document.getElementById('actividades')
				if(!elem) return elem.innerHTML = response
			}
		)

	const component = render()
	return component
}

export default Actividades
