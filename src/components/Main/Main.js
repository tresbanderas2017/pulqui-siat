import yo from 'yo-yo'

import config from '../../config'

function Main() {

	function render() {

		return(
			<div id="main">
				<p class="title">Bienvenidos</p>
			</div>
		)
	}

	/*
	fetch(`${config.menuContenido}/${state.nivel}`)
		.then(jsonResponse => jsonResponse.json() )
		.then(responseMenu => {

				state.info = responseMenu.info

				const elem = document.getElementById("main")
				const updated = render(state)
				yo.update(elem, updated)

			}
		)
	*/

	const component = render()
	return component
}

export default Main
