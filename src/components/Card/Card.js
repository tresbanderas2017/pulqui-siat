function Card(props) {

	function render() {
		return(
			<div style="width: 100%; min-height: 0px" class="mdl-card mdl-shadow--2dp">
				<div style="display: flex; flex-direction: column; align-items: flex-start" class="mdl-card__title">
					{props.children}
				</div>
			</div>
		)
	}

	const component = render()
	return component
}

export default Card
