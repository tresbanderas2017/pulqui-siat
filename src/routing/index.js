import { router } from '../../lib/pulqui.min.js'

import Main from '../components/Main/Main'
import Actividades from '../components/Actividades/Actividades'
import Noticias from '../components/Noticias/Noticias'

router.on('/pulqui2/', (params) => <Main /> )
router.on('/pulqui2/#actividades', (params) => <Actividades /> )
router.on('/pulqui2/#noticias', (params) => <Noticias /> )

function route2Component(route) {
	console.log('CR ', route)

	const component = route === '/pulqui2/#actividades'? <Actividades />:
										route === '/pulqui2/#noticias'? <Noticias />:
										route === '/pulqui2/'? <Main />:
										<Main />

	return component
}

export {
	route2Component
}
