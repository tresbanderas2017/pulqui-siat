import config from '../config/'


function convertCoordsToMuni(lat, lng, cb) {
	fetch(`${config.coords2Muni}/${lat}/${lng}`)
			.then(jsonResponse => jsonResponse.json() )
			.then(response => {
				if(!response.ok) return cb({msg: 'Imposible determinar ubicación'}, undefined)
				cb(undefined, response)
			}
		)
}

export { convertCoordsToMuni }
