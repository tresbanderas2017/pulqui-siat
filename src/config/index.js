//const serverIp = 'http://localhost:8093'
const serverIp = 'http://200.7.138.19:8080/pulquiSIAT/'

export default {
	serverIp: serverIp,
	jspNoticias: `${serverIp}/paginasAjax/noticias.jsp`,
	jspActividades: `${serverIp}/paginasAjax/actividades.jsp`
}
